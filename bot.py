import discord
from discord.ext import commands
import os

TOKEN = os.getenv('DBOT2')

description = '''Archive Bot'''
bot = commands.Bot(command_prefix='?', description=description)

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

bot.remove_command('help')

@bot.command()
async def help(ctx):
    embed = discord.Embed(title="Archive Bot Help", description="Up to date HTML archives")
    embed.add_field(name="Commands", value="Commands right now are a WIP, right now you can hope for a ?export command")
    embed.set_footer(text="Src Code: https://gitlab.com/sysnomid/devguildweb-archivebot/-/tree/master")
    await ctx.send(embed=embed)

bot.run(TOKEN)


